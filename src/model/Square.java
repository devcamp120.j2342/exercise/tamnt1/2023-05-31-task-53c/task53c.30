﻿package model;

public class Square extends Rectangles {
    private double side = 1.0;

    public Square(String color, boolean filled, double side) {
        super(color, filled, side, side);
        this.side = side;
    }

    public Square(double side) {
        super(side, side);
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
        super.setWidth(side);
        super.setHeight(side);
    }

    @Override
    public String toString() {
        return "Square [Rectangle[Shape[" + super.toString() + "], side=" + side + "]]";
    }

}
