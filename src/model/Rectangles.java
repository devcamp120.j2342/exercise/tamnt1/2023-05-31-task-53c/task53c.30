﻿package model;

public class Rectangles extends Shape {
    private double width = 1.0;
    private double height = 1.0;

    public Rectangles() {

    }

    public Rectangles(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public Rectangles(String color, boolean filled, double width, double height) {
        super(color, filled);
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public double getArea() {
        return width * height;
    }

    @Override
    public double getPerimeter() {
        return 2 * (width + height);
    }

    @Override
    public String toString() {
        return "Reactangle [Shape[" + super.toString() + "], width=" + this.width + ", height=" + this.height + "]";
    }

}
