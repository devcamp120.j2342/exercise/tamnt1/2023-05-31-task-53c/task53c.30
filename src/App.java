import model.Circle;
import model.Square;
import model.Rectangles;

public class App {
    public static void main(String[] args) throws Exception {

        Circle cirle3 = new Circle(3.0, "green", true);
        System.out.println(cirle3.getArea() + " and " + cirle3.getPerimeter());
        Rectangles rectangle3 = new Rectangles("green", true, 2.5, 1.5);

        System.out.println(rectangle3.getArea() + " and " + rectangle3.getPerimeter());
        Square square1 = new Square("green", true, 2.0);

        System.out.println(square1);
    }
}
